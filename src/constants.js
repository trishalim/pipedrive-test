export const ApiToken = '703e6721d7c1f51e8a56cf2d619c2d5d6aa80b04';
export const ApiUrl = 'https://api.pipedrive.com/v1/';

export const PersonGroupsKey = '5530730fbc119b11b6fc8b0fbae7941798a2df41';
export const PersonLocationKey = '5bc551c3379fdc95bf6a2b7f3ab4c7a975a3a190';
export const PersonAssistantKey = 'a723b6b9aa9005af983486de3ae989f75d429cf5';
export const PersonOrderKey = 'e5ecbdc732dd55ce429c309d68dbddec1ca07ef7';

import React, { Component } from 'react';

import {
  PersonGroupsKey, PersonLocationKey, PersonAssistantKey
} from '../../constants';
import PersonPicture from '../PersonPicture/PersonPicture';
import './PersonDetails.scss';

class PersonDetails extends Component {

  render() {
    const phoneNumbers = this.props.person.phone.map((phone, index) =>
      <div key={index}>{phone.value}</div>
    );
    const emails = this.props.person.email.map((email, index) =>
      <div key={index}>{email.value}</div>
    );

    return (
      <div className="person-details-container">
        <div className="person-icon">
          <PersonPicture
            pictureId={this.props.person.picture_id}
            firstName={this.props.person.first_name}
            lastName={this.props.person.last_name}
            size="medium"
          />
        </div>
        <div className="name">{this.props.person.name}</div>
        <div className="phone">{phoneNumbers}</div>

        <div className="separator"></div>

        <div className="row">
          <div className="col-4 label">Email</div>
          <div className="col-8 value">{emails}</div>
        </div>
        <div className="row">
          <div className="col-4 label">Organization</div>
          <div className="col-8 value">{this.props.person.org_name}</div>
        </div>
        <div className="row">
          <div className="col-4 label">Assistant</div>
          <div className="col-8 value">{this.props.person[PersonAssistantKey]}</div>
        </div>
        <div className="row">
          <div className="col-4 label">Groups</div>
          <div className="col-8 value">{this.props.person[PersonGroupsKey]}</div>
        </div>
        <div className="row">
          <div className="col-4 label">Location</div>
          <div className="col-8 value">{this.props.person[PersonLocationKey]}</div>
        </div>
      </div>
    );
  }
}

export default PersonDetails;

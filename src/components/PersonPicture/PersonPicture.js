import React, { Component } from 'react';

import './PersonPicture.scss';

class PersonPicture extends Component {

  render() {
    const image = this.props.pictureId ?
      <img
        className={'person-picture__image person-picture__image--' + this.props.size}
        alt={`${this.props.firstName} ${this.props.lastName}`}
        src={this.props.pictureId.pictures['128']}
      /> :
      <div className="person-picture__placeholder">
        {this.props.firstName.charAt(0)}
        {this.props.lastName.charAt(0)}
      </div>;

    return (
      <div className="person-picture">
        {image}
      </div>
    );
  }
}

export default PersonPicture;

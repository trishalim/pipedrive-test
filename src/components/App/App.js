import React, { Component } from 'react';

import Header from '../Header/Header';
import PersonList from '../PersonList/PersonList';

import './App.css';

class App extends Component {
  render() {
    return (
      <div>
        <Header/>
        <PersonList/>
      </div>
    );
  }
}

export default App;

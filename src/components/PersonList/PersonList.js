import React, { Component } from 'react';
import axios from 'axios';
import { DragDropContext, Droppable } from 'react-beautiful-dnd';

import { ApiToken, ApiUrl, PersonOrderKey } from '../../constants';

import Person from '../Person/Person';
import PaginationControls from '../PaginationControls/PaginationControls';

import './PersonList.scss';

class PersonList extends Component {

  personApiUrl = ApiUrl + 'persons';

  constructor() {
    super();
    this.state = {
      persons: [],
      pagination: {
        limit: 5,
        more_items_in_collection: false,
        next_start: 0,
        start: 0,
      },
    };
  }

  componentDidMount() {
    this.fetchPersons();
  }

  fetchPersons(start = this.state.pagination.next_start) {
    const config = {
      params: {
        api_token: ApiToken,
        sort: PersonOrderKey,
        start: start,
        limit: this.state.pagination.limit,
      },
    };
    axios.get(this.personApiUrl, config)
      .then(response => {
        this.setState({
          persons: response.data.data,
          pagination: response.data.additional_data.pagination
        });
      });
  }

  onDragEnd(dragEndEvent) {
    // event has no destination
    // element was dragged outside of a droppable
    if (!dragEndEvent.destination) {
      return;
    }

    // move dragged element to its new position in the list
    this.reorderPersons(
      dragEndEvent.source.index,
      dragEndEvent.destination.index
    );
  }

  reorderPersons(sourceIndex, destinationIndex) {
    const persons = this.state.persons;

    // remove element from its original position
    const [dragElement] = persons.splice(sourceIndex, 1);
    // add element to its new position
    persons.splice(destinationIndex, 0, dragElement);

    // index where we start to iterate the loop
    const startIndex = destinationIndex > sourceIndex ? sourceIndex : destinationIndex;
    // index where the loop ends
    const endIndex = destinationIndex > sourceIndex ? destinationIndex : sourceIndex;

    // modify "order" field of the persons affected during the sort order change
    for (let index = startIndex; index <= endIndex; index++) {
      const person = persons[index];
      const order = index + this.state.pagination.start;

      // modify person in client side
      persons[index][PersonOrderKey] = order;

      // modify person in server side
      const requestUrl = `${this.personApiUrl}/${person.id}?api_token=${ApiToken}`;
      const body = {};
      body[PersonOrderKey] = order;
      // API call to modify person
      axios.put(requestUrl, body);
    }

    this.setState({ persons });
  }

  goToNextPage() {
    this.fetchPersons();
  }

  goToPreviousPage() {
    this.fetchPersons(this.state.pagination.start - this.state.pagination.limit);
  }

  render() {
    return (
      <div>
        <h5 className="title">People's List</h5>
        <div className="separator"></div>
        <DragDropContext onDragEnd={this.onDragEnd.bind(this)}>
          <Droppable droppableId="person-list">
            {(provided) => (
              <div className="person-list"
                ref={provided.innerRef}>
                {this.state.persons.map((person, index) => (
                  <Person
                    className="person"
                    index={index}
                    key={person.id}
                    person={person}
                  />
                ))}
              </div>
            )}
          </Droppable>
        </DragDropContext>
        <PaginationControls
          hasPreviousPage={this.state.pagination.start > 0}
          hasNextPage={this.state.pagination.more_items_in_collection}
          goToNextPage={this.goToNextPage.bind(this)}
          goToPreviousPage={this.goToPreviousPage.bind(this)}
        />
      </div>
    );
  }
}

export default PersonList;

import React, { Component } from 'react';

import logo from '../../assets/logo.svg';
import './Header.scss';

class Header extends Component {
  render() {
    return (
      <div className="header">
        <img src={logo} alt="Pipedrive logo" className="logo"/>
      </div>
    );
  }
}

export default Header;

import React, { Component } from 'react';
import { Draggable } from 'react-beautiful-dnd';

import PersonModal from '../PersonModal/PersonModal';
import PersonPicture from '../PersonPicture/PersonPicture';

import './Person.scss';

class Person extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isModalOpen: false
    };

    this.toggleModal = this.toggleModal.bind(this);
  }

  toggleModal() {
    this.setState({
      isModalOpen: !this.state.isModalOpen
    });
  }

  render() {
    return (
      <div>

        <Draggable key={this.props.person.id}
          draggableId={this.props.person.id}
          index={this.props.index}>
          {(provided) => (
            <div
              ref={provided.innerRef}
              {...provided.draggableProps}
              {...provided.dragHandleProps}
            >
              <div className="person d-flex justify-content-between align-items-center"
                onClick={this.toggleModal}>
                <div>
                  <div className="name">{this.props.person.name}</div>
                  <div className="org">
                    <i className="fas fa-building org__icon"></i>
                    <span className="org__name">{this.props.person.org_name}</span>
                  </div>
                </div>
                <PersonPicture
                  pictureId={this.props.person.picture_id}
                  firstName={this.props.person.first_name}
                  lastName={this.props.person.last_name}
                  size="small"
                />
              </div>
            </div>
          )}
        </Draggable>

        <PersonModal
          person={this.props.person}
          isModalOpen={this.state.isModalOpen}
          toggleModal={this.toggleModal.bind(this)}
        />

      </div>
    );
  }
}

export default Person;

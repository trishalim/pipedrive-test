import React, { Component } from 'react';
import { Button } from 'reactstrap';

import './PaginationControls.scss';

class PaginationControls extends Component {

  render() {
    return (
      <div className="pagination-controls d-flex justify-content-end">
        <Button
          disabled={!this.props.hasPreviousPage}
          onClick={this.props.goToPreviousPage}>
          Previous
        </Button>
        <Button
          disabled={!this.props.hasNextPage}
          onClick={this.props.goToNextPage}>
          Next
        </Button>
      </div>
    );
  }

}

export default PaginationControls;

import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

import PersonDetails from '../PersonDetails/PersonDetails';

class PersonModal extends Component {

  render() {
    return (
      <Modal isOpen={this.props.isModalOpen} toggle={this.props.toggleModal}>
        <ModalHeader>
          <div className="w-100 d-flex justify-content-between">
            Person Information
            <button className="close" onClick={this.props.toggleModal}>×</button>
          </div>
        </ModalHeader>
        <ModalBody>
          <PersonDetails
            person={this.props.person}
          />
        </ModalBody>
        <ModalFooter>
          <Button className="back-button" onClick={this.props.toggleModal}>Back</Button>
        </ModalFooter>
      </Modal>
    );
  }
}

export default PersonModal;
